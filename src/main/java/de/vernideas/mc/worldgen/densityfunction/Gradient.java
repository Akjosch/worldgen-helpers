package de.vernideas.mc.worldgen.densityfunction;

import java.util.Locale;
import java.util.Objects;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.DynamicOps;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.PrimitiveCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import de.vernideas.mc.worldgen.Util;
import net.minecraft.util.dynamic.CodecHolder;
import net.minecraft.world.gen.densityfunction.DensityFunction;

/**
 * Density function calculating a smooth gradient from 0.0 to 1.0 in the XZ plane.
 * <br><br>
 * <strong>ID:</strong><code>"akj_worldgen:gradient"</code>
 * <br><br>
 * Parameter:
 * <dl>
 * <dt>"direction"</dt><dd>Data type: double or a pre-defined string<br>
 *   The direction of the gradient, in degrees, counter-clockwise from 0.0 towards the "right"
 *   (positive X values)<br>
 *   String values for common directions are available:<br>
 *   <ul>
 *     <li>"x", "+x", "e" = 0.0</li>
 *     <li>"ne" = 45.0</li>
 *     <li>"z", "+z", "n" = 90.0</li>
 *     <li>"nw" = 135.0</li>
 *     <li>"-x", "w" = 180.0</li>
 *     <li>"sw" 0 225.0</li>
 *     <li>"-z", "s" = 270.0</li>
 *     <li>"se" = 315.0</li>
 *   </ul></dd>
 * <dt>"from"</dt><dd>Data type: double<br>
 *   The distance from the origin the (primary) gradient starts, from -1000000000.0 to 1000000000.0</dd>
 * <dt>"to"</dt><dd>Data type: double<br>
 *   The distance from the origin the (primary) gradient ends, from -1000000000.0 to 1000000000.0</dd>
 * <dt>"repeat"</dt><dd>Data type: pre-defined string<br>
 *   Optional; default value "single"<br>
 *   Possible values:<br>
 *   <ul>
 *     <li>"single" - 0.0 at "from" and before and 1.0 at "end" and after (alternative name: "none")</li>
 *     <li>"double" - 0.0 at "from" and before and at "end" and after; 1.0 in the exact middle</li>
 *     <li>"triangular" - 0.0 at "from" and all even multiples of the total distance between "from" and "to", 1.0 at "to" and similarly at all even multiples</li>
 *     <li>"sawtooth" - as "single", but repeats itself indefinitely in both directions</li>
 *   </ul></dd>
 * <dt>Return value</dt><dd>between 0.0 and 1.0, depending on the chosen "repeat"</dd>
 * </dl>
 * <br><br>
 * See also: <code>"minecraft:spline"</code> for a flexible way to map the results into non-linear shapes,
 * and <code>"minecraft:y_clamped_gradient"</code> for a gradient in the Y direction
 */
public final class Gradient implements DensityFunction.Base {
	public static final MapCodec<Gradient> GRADIENT_CODEC = RecordCodecBuilder.mapCodec(
			(instance) -> instance.group(
					Util.DIRECTION_CODEC.fieldOf("direction").forGetter(Gradient::direction),
					Codec.doubleRange(-1000000000.0, 1000000000.0).fieldOf("from").forGetter(Gradient::from),
					Codec.doubleRange(-1000000000.0, 1000000000.0).fieldOf("to").forGetter(Gradient::to),
					GradientForm.CODEC.optionalFieldOf("repeat", GradientForm.SINGLE).forGetter(Gradient::form)).apply(instance, Gradient::new));
	private static final CodecHolder<? extends DensityFunction> CODEC_HOLDER = CodecHolder.of(GRADIENT_CODEC);
	
	private final double direction;
	private final double from;
	private final double to;
	private final GradientForm form;
	/* precalculated transformation parameters for rotation, scale and shift */
	private final transient double xmul;
	private final transient double ymul;
	private final transient double shift;
	
	public Gradient(double direction, double from, double to, GradientForm form) {
		this.direction = direction;
		if(from >= to) {
            throw new IllegalArgumentException(String.format("From larger than to: %f > %f", from, to));
		}
		this.from = from;
		this.to = to;
		this.form = form;
		this.xmul = Math.cos(Math.toRadians(direction)) / (to - from);
		this.ymul = - Math.sin(Math.toRadians(direction)) / (to - from);
		this.shift = - from  / (to - from);
	}
	
	public Gradient(double direction, double from, double to) {
		this(direction, from, to, GradientForm.SINGLE);
	}
	
	public double direction() { return this.direction; }
	public double from() { return this.from; }
	public double to() { return this.to; }
	public GradientForm form() { return this.form; }
	
	@Override public double sample(NoisePos pos) {
		return form.value(xmul * pos.blockX() + ymul * pos.blockZ() + shift);
	}

	@Override public double minValue() {
		return 0.0;
	}

	@Override public double maxValue() {
		return 1.0;
	}

	@Override public CodecHolder<? extends DensityFunction> getCodecHolder() {
		return CODEC_HOLDER;
	}
	
	@Override public int hashCode() {
		return Objects.hash(direction, form, from, to);
	}

	@Override public boolean equals(Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		Gradient other = (Gradient) obj;
		return Double.doubleToLongBits(direction) == Double.doubleToLongBits(other.direction) && form == other.form
				&& Double.doubleToLongBits(from) == Double.doubleToLongBits(other.from)
				&& Double.doubleToLongBits(to) == Double.doubleToLongBits(other.to);
	}

	static enum GradientForm {
		SINGLE, DOUBLE, TRIANGULAR, SAWTOOTH;
		
		public static final Codec<GradientForm> CODEC = new PrimitiveCodec<GradientForm>() {

			@Override public <T> DataResult<GradientForm> read(DynamicOps<T> ops, T input) {
				return ops.getStringValue(input).map(GradientForm::fromString);
			}

			@Override public <T> T write(DynamicOps<T> ops, GradientForm value) {
				return ops.createString(value.toString().toLowerCase(Locale.ROOT));
			}
			
			@Override public String toString() {
				return "GradientForm";
			}
		};
		
		public static GradientForm fromString(String val) {
			switch(val.toUpperCase(Locale.ROOT)) {
				case "NONE":
					return GradientForm.SINGLE;
				default:
					return GradientForm.valueOf(val.toUpperCase(Locale.ROOT));
			}
		}
		
		public double value(final double at) {
			switch(this) {
				case SINGLE:
					return Util.clamp(at, 0.0, 1.0);
				case DOUBLE:
					return Util.clamp(1.0 - Math.abs(at - 0.5) * 2.0, 0.0, 1.0);
				case TRIANGULAR:
					return Math.abs(1.0 - Math.abs(at) % 2.0);
				case SAWTOOTH:
					double sawAt = at % 1.0;
					return sawAt >= 0.0 ? sawAt : 1.0 + sawAt;
			}
			return Double.NaN;
		}
	}

}
