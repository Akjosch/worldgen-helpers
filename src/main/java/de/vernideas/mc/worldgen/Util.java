package de.vernideas.mc.worldgen;

import java.util.Locale;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.DynamicOps;
import com.mojang.serialization.codecs.PrimitiveCodec;

public final class Util {
	public static final Codec<Double> DIRECTION_CODEC = new PrimitiveCodec<Double> () {
		@Override public <T> DataResult<Double> read(DynamicOps<T> ops, T input) {
			final DataResult<Number> numValue = ops.getNumberValue(input);
			if(numValue.error().isEmpty()) {
				return numValue.map(Number::doubleValue).map(Util::limitRotation);
			}
			return ops.getStringValue(input).map(Util::directionToDouble);
		}

		@Override public <T> T write(DynamicOps<T> ops, Double value) {
            return ops.createDouble(value);
		}
		
        @Override public String toString() {
            return "Direction";
        }
	};
	
	public static Double directionToDouble(final String dir) {
		switch(dir.toLowerCase(Locale.ROOT)) {
			case "x": case "+x": case "e": return 0.0;
			case "ne": return 45.0;
			case "z": case "+z": case "n": return 90.0;
			case "nw": return 135.0;
			case "-x": case "w": return 180.0;
			case "sw": return 225.0;
			case "-z": case "s": return 270.0;
			case "se": return 315.0;
			default: return 0.0;
		}
	}
	
	public static double limitRotation(final double rot) {
		final double result = rot % 360.0;
		return result >= 0.0 ? result : 360.0 + result;
	}
	
	public static double clamp(double val, double from, double to) {
		if(val <= from) {
			return from;
		} else if(val >= to) {
			return to;
		} else {
			return val;
		}
	}
	
	/* no instancing */
	private Util() {}
}
