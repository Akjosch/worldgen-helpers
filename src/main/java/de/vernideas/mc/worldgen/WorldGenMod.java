package de.vernideas.mc.worldgen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.vernideas.mc.worldgen.densityfunction.Distance;
import de.vernideas.mc.worldgen.densityfunction.Gradient;
import de.vernideas.mc.worldgen.densityfunction.Lerp;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.ModInitializer;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class WorldGenMod implements ModInitializer, ClientModInitializer {
    public static final Logger LOGGER = LoggerFactory.getLogger("akj_worldgen");

	@Override public void onInitialize() {
		Registry.register(Registries.DENSITY_FUNCTION_TYPE, new Identifier("akj_worldgen", "distance"), Distance.DISTANCE_CODEC.codec());
		Registry.register(Registries.DENSITY_FUNCTION_TYPE, new Identifier("akj_worldgen", "lerp"), Lerp.LERP_CODEC.codec());
		Registry.register(Registries.DENSITY_FUNCTION_TYPE, new Identifier("akj_worldgen", "gradient"), Gradient.GRADIENT_CODEC.codec());
	}

	@Override public void onInitializeClient() {
		// TODO Auto-generated method stub
		
	}

}
