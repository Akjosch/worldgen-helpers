This mod adds a few low-level helper classes for Minecraft's world generation, as well as examples of their use in the form of world presets.

More [on the mod's wiki page](../../wikis/Overview).